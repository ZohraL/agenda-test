import { expect } from "chai";
import { EventController } from "../../src/controller/EventController";
import { Event } from "../../src/entity/Event";

describe("Event", () => {
    let eventController: EventController;

    beforeEach(() => {
        eventController = new EventController();
    });

    describe("Get events from DB", () => {
        it("Should get all events", async () => {
            let totalEventsLength: number;
            await eventController.getAll().then((res) => {
                totalEventsLength = res.length;
            });
            // Après passage de script, la BDD contient par default 6 events
            expect(totalEventsLength).to.be.equal(6);
        });
    });

    describe("Add event", () => {
        it("Should allow the user to add an event into the DB", async () => {
            // 1.On récupère le nb d'events actuellement en base
            let repoLength: number;
            await eventController.getAll().then((res) => {
                repoLength = res.length;
            });
            // 2.On crée un nouvel event
            let newEvent = new Event(
                "La condition des poules pondeuses en Laponie",
                "Débat sur un sujet houleux qui touche tout un chacun",
                new Date("January 03, 2021 17:30:00"),
                new Date("January 03, 2021 19:00:00"),
                "Mairie du 1er",
                50,
                3,
                "Social et solidaire"
            );
            // 3.On l'ajoute dans la base
            eventController.addEvent(newEvent);
            // On récupère le nouveau nb d'event présents en base
            let newLength: number;
            await eventController.getAll().then((res) => {
                newLength = res.length;
            });

            // 4.on s'attend à ce qu'un résultat ait été ajouté en base
            expect(newLength).to.be.equal(repoLength + 1);
        });
    });

    describe('Delete Event', () => {
        it('Should delete event by its id', async () => {
            // 1.On récupère la taille du tableau d'event qui se trouve dans la BDD
            let actualEventsLength: number;
            await eventController.getAll().then((res) => {
                actualEventsLength = res.length;
            });

            // 2. On supprime un event par son Id
            eventController.deleteEvent(5);

            // 3.On récupère la nouvelle taille du tableau d'event
            let newEventsLength: number;
            await eventController.getAll().then((res) => {
                newEventsLength = res.length;
            });

            // 4. On compare les deux résultats
            expect(newEventsLength).to.be.equal(actualEventsLength - 1);
        });
    });

    describe('Modify Event', () => {
        it('Should modify event by its id', async () => {
            let eventBefore: Event;

            await eventController.getById(5).then((event) => {
                eventBefore = event;
            });

            let eventAfter: Event;

            await eventController.modifyEvent(5,
                "Il y a des pigeons a Lyon",
                new Date("2021-11-05T10:00:00"),
                new Date("2021-11-06T16:00:00"),
                "Place Jean Macé",
                18,
                "Alcool").then((res) => {
                    eventAfter = res;
                });

            expect(eventBefore).to.not.be.equal(eventAfter);
        });
    });

    describe("Find By", () => {
        it("Should find Events by category", async () => {
            let listEventsByCategory: Event[];

            await eventController.getEventByCategory("Make-up").then((res) => {
                listEventsByCategory = res;
            });

            expect(listEventsByCategory[0]).to.not.be.null;
        });


        it('Should find Events by eventPlanner', async () => {
            let listEventsByEventPlanner: Event[];
            await eventController.getByEventPlanner(6).then((res) => {
                listEventsByEventPlanner = res;
            });
            expect(listEventsByEventPlanner[0]).to.not.be.null;
        });
    });
});
