import { expect } from "chai";
import { UserController } from "../../src/controller/UserController";
import { User } from "../../src/entity/User";

describe('UserController', () => {

    let user: User;
    let userController: UserController;
    beforeEach(() => {
        user = new User('toto', 'toto@mail.fr', 'toto1234', 'ADMIN');
        userController = new UserController();
        userController.user = user;
    });


    it('should create a new User with name = MarkZ, email = theboss@fb.com, etc ...', async () => {
        let firstLength:number;
        await userController.findAll().then((result) => {
            firstLength = result.length;
        })
        let userTest = new User('MarkZ', 'theboss@fb.com', 'iamtheboss', 'USER');
        userController.register(userTest);

        let newLength:number;
        await userController.findAll().then((result) => {
            newLength = result.length;
        })

        expect(newLength).to.be.equal(firstLength + 1);
    });


    it('should find users by names', async () => {
        let expected:User[];

        userController.register(user);
        await userController.findByName('toto').then((result) => {
            expected = result;
        });

        expect(expected[0].getEmail()).to.be.equal(user.getEmail());
    });

});
