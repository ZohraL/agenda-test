import { UserController } from './../../src/controller/UserController';
import { expect } from "chai";
import { User } from "../../src/entity/User";

describe('User', () => {
    let user: User;
    let userController: UserController;
    beforeEach(() => {
        user = new User('toto', 'toto@mail.fr', 'toto1234', 'ADMIN');
        userController = new UserController();
        userController.user = user;
    });


    it('should return true if user is connected', () => {
        userController.connect();
        expect(user.isConnected).to.be.true;
    });

    it('should return true if the user is an Admin', () => {
        expect(userController.user.getRole()).to.be.equal('ADMIN');
    });



    it('should find users by names', () => {
        expect(userController.user.getName()).to.be.equal('toto');
    });
})
