DROP DATABASE IF EXISTS semaine_test;

CREATE DATABASE IF NOT EXISTS semaine_test;

USE semaine_test;

CREATE TABLE User (
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, 
    name VARCHAR(64), 
    email VARCHAR(64), 
    password VARCHAR(64),
    isConnected TINYINT,
    role VARCHAR(64)
);

CREATE TABLE Event (
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(64),
    description VARCHAR(250),
    startDate DATETIME,
    endDate DATETIME,
    place VARCHAR(64),
    capacity INT,
    eventPlanner_Id INT,
    category VARCHAR(30),
    CONSTRAINT FK_eventPlanner_Id 
        FOREIGN KEY (eventPlanner_Id) 
        REFERENCES User(Id)
);

INSERT INTO User VALUES(1, 'Palais des Congrès', 'PdC@lyon.com', '1234', true, 'ADMIN');
INSERT INTO User VALUES(2, 'Guy de Maupassant', 'pass@partout.com', 'confession', true, 'USER');
INSERT INTO User VALUES (3, 'Mister Pancho', 'Expert@me.com', '1234', false, 'USER');
INSERT INTO User VALUES (4, 'René-Espoir Aboso', 'REA@me.com', '1234', false, 'USER');
INSERT INTO User VALUES (5, 'David GoodEnough', 'goodenough@me.com', '1234', false, 'USER');
INSERT INTO User VALUES (6, 'Marie-Antoinnette De La Pissaladière', 'marie@versailles.fr', 'louis', true, 'USER');
INSERT INTO User VALUES (7, "Jeanne d'Arc", 'Aufeu@gmal.com', 'aubûcher', false, 'USER');

INSERT INTO Event VALUES (1,"Elevage de phoques au Canada", 
                          "Vous saurez tout sur l'élevage de ces mignons petits mammifères marins", 
                          "2020-12-16T10:30:00", 
                          "2020-12-16T12:30:00", 
                          "Salle Safari au Flagship", 
                          30, 
                          4, 
                          "Make-up");

INSERT INTO Event VALUES(2,"La culture du pois-chiche sur l'île d'Okinawa",
                         "Comment lancer sa multinational agroalimentaire au Japon ",
                         "2021-01-01T14:00:00",
                         "2021-03-01T15:30:00",
                         "Palais des Congrès",
                         250,
                         5,
                         "Le programme socialiste");

INSERT INTO Event VALUES(3,"L'observation des pigeons dans leur milieu naturel",
                        "les pigeons, ces volatiles incroyables, ont beaucoup de choses à nous apprendre",
                        "2021-11-08T08:00:00",
                        "2021-12-24T16:00:00",
                        "Place Bellecour",
                        15,
                        6,
                        "Comment devenir un pigeon");

INSERT INTO Event VALUES(4,"La chasse aux mouches en milieu urbain",
                         "Redécouvrir ses instinct de mouche",
                         "2023-03-04T08:00:00",
                         "2023-03-06T08:00:00",
                         "Place des chèvres",
                         12,
                         3,
                         "Découverte de soi");

INSERT INTO Event VALUES(5,"TED Talk : Boire quand on a soif",
                        "Il n'y a pas de mouette a Lyon",
                        "2021-11-05T10:00:00",
                        "2021-11-06T16:00:00",
                        "Place pouettepouette",
                        18,
                        2,
                        "Alcool");

INSERT INTO Event VALUES (6,"Le chant des baleines Place de la Concorde",
                          "Venez vous relaxer dans un environnement hostile",
                          "2021-10-19T16:00:00",
                          "2021-10-26T17:30:00",
                          "Place de la Concorde",
                          48,
                          7,
                          "Relaxation");

                    