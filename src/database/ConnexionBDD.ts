import { createConnection } from "mysql";
import { promisify } from "util";

export class ConnexionBDD {
  
  private query;
  private bdd = "semaine_test";
  private user = "simplon";
  private password = "1234";
  private host = "localhost";
  public connection;

  //Création de la connexion base de données
  constructor() {
    this.connection = createConnection({
      host: this.host,
      database: this.bdd,
      user: this.user,
      password: this.password,
    });
    this.query = promisify(this.connection.query).bind(this.connection);
  }
  getQuery() {
    return this.query;
  }
}

