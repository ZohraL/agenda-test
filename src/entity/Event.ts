export class Event {
    private id:number
    private title:string;
    private description:string;
    private startDate:Date;
    private endDate:Date;
    private place:string;
    private capacity:number;
    private eventPlanner_Id:number;
    private category:string;

    constructor(title:string, description:string, startDate:Date, endDate:Date,
                place:string,capacity:number,eventPlanner_Id:number,category:string){
        this.title = title,
        this.description = description,
        this.startDate = startDate,
        this.endDate = endDate,
        this.place = place,
        this.capacity = capacity,
        this.eventPlanner_Id = eventPlanner_Id,
        this.category = category
    }

    setId(id:number){
        this.id = id;
    }

    getId() {
        return this.id;
    }

    getTitle(){
        return this.title;
    }

    setDescription(description:string){
        this.description = description;
    }

    getDescription(){
        return this.description;
    }

    setStartDate(startDate:Date){
        this.startDate = startDate;
    }

    getStartDate(){
        return this.startDate;
    }

    setEndDate(endDate:Date){
        this.endDate = endDate;
    }

    getEndDate(){
        return this.endDate;
    }

    setPlace(place:string){
        this.place = place;
    }

    getPlace(){
        return this.place;
    }

    setCapacity(capacity:number){
        this.capacity = capacity;
    }

    getCapacity(){
        return this.capacity;
    }

    getEventPlanner(){
        return this.eventPlanner_Id;
    }

    setCategory(category:string){
        this.category = category;
    }

    getCategory(){
        return this.category;
    }
}