
export class User {
    
  private id: number;

  private name: string;

  private email: string;

  private password: string;

  public isConnected: boolean;

  private role: string;

  constructor(name: string, email: string, password: string, role: string) {
    this.name = name;
    this.email = email;
    this.password = password;
    this.isConnected = false;
    this.role = role;
  }

  setId(id:number){
    this.id = id;
  }

  getId() {
    return this.id;
  }
  getName() {
    return this.name;
  }
  getEmail() {
    return this.email;
  }
  getPassword() {
    return this.password;
  }
  getRole() {
    return this.role;
  }
}
