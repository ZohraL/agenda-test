import { Event } from "../entity/Event";
import { ConnexionBDD } from "../database/ConnexionBDD";

export class EventController{
    private connection;

    constructor() {
        this.connection = new ConnexionBDD();
    }

    async addEvent(event: Event) : Promise<number> {
        let result = await this.connection.query('INSERT INTO Event (title,description,startDate,endDate,place,capacity,eventPlanner_Id,category) VALUES (?,?,?,?,?,?,?,?)', [
            event.getTitle(),
            event.getDescription(),
            event.getStartDate(),
            event.getEndDate(),
            event.getPlace(),
            event.getCapacity(),
            event.getEventPlanner(),
            event.getCategory()
        ]);
        let id = result.insertId;
        event.setId(id);
        return id;        
    }
    
    async getAll(): Promise<Event[]> {
        let result = await this.connection.query('SELECT * FROM Event')
        return result.map((row) => {
            new Event(
                row['title'], 
                row['description'], 
                row['startDate'], 
                row['endDate'], 
                row['place'], 
                row['capacity'], 
                row['eventPlanner_Id'],
                row['category'])
             }); // prend le contenu d'un tab et effectue l'opération qu'on lui dit de faire et renvoie une nvelle version du tab.
        };

    async deleteEvent(id:number): Promise<number> {
        let result = await this.connection.query('DELETE FROM Event WHERE id = ? ', [id])
        return result.id;
    }

    async modifyEvent(id:number, description?:string, startDate?:Date, endDate?:Date, place?:string, capacity?:number, category?:string):Promise<Event>{
        let result = await this.connection.query('UPDATE Event SET description=?,startDate=?,endDate=?,place=?,capacity=?,category=? WHERE id = ?', [
            id, description, startDate, endDate, place, capacity,category
            
        ])
        return result;
    }

    async getById(id:number): Promise<Event> {
        let result = await this.connection.query('SELECT * FROM Event WHERE id = ? ', [id])
        return result;
    }

    async getEventByCategory(category:string):Promise<Event[]> {
        let result = await this.connection.query('SELECT * FROM Event WHERE category = ?', [category])
        return result.map((row) => {
            new Event(
                row['title'], 
                row['description'], 
                row['startDate'], 
                row['endDate'], 
                row['place'], 
                row['capacity'], 
                row['eventPlanner_Id'],
                row['category'])
             });
    }

    async getByEventPlanner(idEventPlanner:number): Promise<Event[]> {
        let result = await this.connection.query('SELECT e.*, u.name FROM Event e JOIN User u ON u.id = e.eventPlanner_Id WHERE e.eventPlanner_Id = ?', [idEventPlanner])
        return result.map((row) => {
            new Event(
                row['title'], 
                row['description'], 
                row['startDate'], 
                row['endDate'], 
                row['place'], 
                row['capacity'], 
                row['eventPlanner_Id'],
                row['category'])
             });
    }
}