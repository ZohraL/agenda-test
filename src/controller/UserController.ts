import { ConnexionBDD } from "../database/ConnexionBDD";
import { User } from "../entity/User";

export class UserController {
  public user: User;
  private connection;

  constructor() {
    this.connection = new ConnexionBDD();
  }
  async findAll(): Promise<User[]> {

    let result = await this.connection.query('SELECT * FROM User')
    return result.map(row => new User(row['name'], row['email'], row['password'], row['role'])); // prend le contenu d'un tab et effectue l'opération qu'on lui dit de faire et renvoie une nvelle version du tab.
  }

  async findByName(name: string): Promise<User[]> {

    let result = await this.connection.query('SELECT * FROM User WHERE name = ?', name)
    return result.map(row => new User(row['name'], row['email'], row['password'], row['role']));;
  }

  async register(user: User): Promise<number> {
    let result = await this.connection.query('INSERT INTO User (name,email,password,role) VALUES (?,?,?,?)', [
      user.getName(),
      user.getEmail(),
      user.getPassword(),
      user.getRole(),
    ]);
    let id = result.insertId;
    user.setId(id);
    return id;
  }

  connect() {
    this.user.isConnected = true;
  }
}
