## Contexte
L'objectif est de réaliser un agenda partagé qui permettra à une collectivité, ou une association de permettra à ses membres/adhérents/communauté de rajouter des événements sur un calendrier qui pourra être consulter par n'importe qui.



## User Stories

1. En tant qu'association je veux pouvoir avoir a possibilité de modérer les events en pouvant les supprimés afin de garantir une bonne gestion
   1. Etre connecté
   
   2. Avoir un profil administrateur

   3. Avoir des évènements prévus 
   
      
   
2. En tant qu'association, je veux que chacunes des plages horaires ne propose qu'un seul événement afin de pouvoir adapter la taille des locaux aux nb de participants
   
1. Savoir si la plage horaire est disponible ou non
   
      
   
3. En tant qu'utilisateur, je veux pouvoir m'inscrire/me désinscrire d'un évènement afin de pouvoir gérer mon temps comme je le souhaite
   
   1. Etre connecté
   
2. Etre inscrit à un évènement
   
      
   
4. En tant qu'adhérent (utisateurs connectés) je veux pouvoir ajouter des indicateurs sur mes évènements afin de les catégoriser par type d'évenements
   1. Etre connecté
   
   2. Etre en train de créer un évènement

   3. Avoir des catégories prédéfini par l'administrateur
   
      
   
5. En tant qu'utilisateur je veux pouvoir filtrer facilement les évènements afin de pouvoir visualiser uniquement le type d'évènement qui m'interesse.

   1. Avoir un critère de recherche (par type, par date,...)

      

6. En tant qu'utilisateur, je veux pouvoir cliquer sur un événement dans le calendrier pour en voir le détail afin de voir si l'évènement m'interesse

   1. L'évènement existe

      

7. En tant qu'utilisateur je veux pouvoir choisir le  laps de temps (par mois/semaine/jour) affiché par l'agenda afin d'avoir un maximum de visibilité sur mon emploi du temps

   

8. En tant qu'adhérent je souhaiterais avoir la possibilité d'ajouter un rappel sur un évènement afin d'être prévenu dans un laps de temps que j'aurais défini
   1. Etre connecté 

   2. Etre inscrit à un évènement

   3. Avoir défini un rappel 

      

9. En tant qu'association je souhaiterais que seul les adhérents (utilisateurs connectés)  puisse ajouter des évènements afin de mieux modérer le calendrier

   

10. En tant qu'assocation je souhaite que les utilisateurs puissent créer un compte rapidement afin qu'il soit en mesure de gérer des évènements
    1. Avoir un pseudo

    2. Avoir une adresse mail unique

    3. Avoir un mot de passe

       

## Maquettes fonctionnelles

![](./maquettes/US-1.png)

![](./maquettes/US-2.png)

![](./maquettes/US-3.png)

![](./maquettes/US-3b.png)

![](./maquettes/US-4.png)

![](./maquettes/US-5.png)

![](./maquettes/US-6.png)

![](./maquettes/US-10.png)

![](./maquettes/US-10b.png)



## Use Case

[Diagrammes de Use Case](https://gitlab.com/ZohraL/agenda-test/-/tree/master/Use%20case%20)

